//
//  DataFormatingSortingTests.swift
//  BittrackerTests
//
//  Created by Nissi Vieira Miranda on 28/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import XCTest
@testable import Bittracker

class DataFormatingSortingTests: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testDateStringSorting() {
        
        let toSort = ["2019-05-17", "2019-05-21", "2019-05-12", "2019-05-25", "2019-05-13", "2019-05-19", "2019-05-14", "2019-05-18", "2019-05-15", "2019-05-16", "2019-05-23", "2019-05-20", "2019-05-22", "2019-05-24"]
        let expected = ["2019-05-25", "2019-05-24", "2019-05-23", "2019-05-22", "2019-05-21", "2019-05-20", "2019-05-19", "2019-05-18", "2019-05-17", "2019-05-16", "2019-05-15", "2019-05-14", "2019-05-13", "2019-05-12"]
        let result = DateStringSorter.sort(toSort)
        
        XCTAssertTrue(result.elementsEqual(expected))
    }

    func testDateUrlFormatting() {
        
        let dateUrl = DateFormater.getUrlFormat(Date())
        
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
        
        XCTAssertNotNil(dateFormatter.date(from: dateUrl))
    }
}
