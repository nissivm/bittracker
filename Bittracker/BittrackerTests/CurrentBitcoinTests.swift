//
//  CurrentBitcoinTests.swift
//  BittrackerTests
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import XCTest
import BittrackerCurrent
@testable import Bittracker

class CurrentBitcoinTests: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testCurrentBitcoin() {
        
        let expec = expectation(description: "Get current bitcoin rate")
        
        CurrentBitcoin.get(completion: {
            
            () -> Void in
            
            expec.fulfill()
            XCTAssertTrue(CurrentBitcoin.success)
        })
        
        waitForExpectations(timeout: 5, handler: {
            (error) -> Void in
            XCTAssertTrue(error == nil)
        })
    }
}
