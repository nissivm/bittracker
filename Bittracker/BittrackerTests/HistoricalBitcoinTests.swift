//
//  HistoricalBitcoinTests.swift
//  BittrackerTests
//
//  Created by Nissi Vieira Miranda on 28/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import XCTest
@testable import Bittracker

class HistoricalBitcoinTests: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testHistoricalBitcoin() {
        
        let expec = expectation(description: "Get historical bitcoin rate")
        
        HistoricalBitcoin.get(completion: {
            
            () -> Void in
            
            expec.fulfill()
            XCTAssertTrue(HistoricalBitcoin.eurData.count == 2)
            XCTAssertTrue(HistoricalBitcoin.gbpData.count == 2)
            XCTAssertTrue(HistoricalBitcoin.usdData.count == 2)
            XCTAssertTrue(HistoricalBitcoin.eurData[0].count > 0)
            XCTAssertTrue(HistoricalBitcoin.eurData[1].count > 0)
            XCTAssertTrue(HistoricalBitcoin.gbpData[0].count > 0)
            XCTAssertTrue(HistoricalBitcoin.gbpData[1].count > 0)
            XCTAssertTrue(HistoricalBitcoin.usdData[0].count > 0)
            XCTAssertTrue(HistoricalBitcoin.usdData[1].count > 0)
        })
        
        waitForExpectations(timeout: 5, handler: {
            (error) -> Void in
            XCTAssertTrue(error == nil)
        })
    }
}
