//
//  TodayViewController.swift
//  BittrackerToday
//
//  Created by Nissi Vieira Miranda on 27/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import NotificationCenter
import BittrackerCurrent

enum Rate: String {
    case Eur = "EurRate"
    case Gbp = "GbpRate"
    case Usd = "UsdRate"
}

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var eurRateLabel: UILabel!
    @IBOutlet weak var gbpRateLabel: UILabel!
    @IBOutlet weak var usdRateLabel: UILabel!
    
    private let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(updateUI),
        name: Notification.Name(RateUpdater.notification),
        object: nil)
        
        if let eur = defaults.string(forKey: Rate.Eur.rawValue) {
            eurRateLabel.text = eur
        }
        
        if let gbp = defaults.string(forKey: Rate.Gbp.rawValue) {
            gbpRateLabel.text = gbp
        }
        
        if let usd = defaults.string(forKey: Rate.Usd.rawValue) {
            usdRateLabel.text = usd
        }
        
        RateUpdater.startUpdating(interval: 10)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateUI() {
        
        eurRateLabel.text = CurrentBitcoin.eurRate
        gbpRateLabel.text = CurrentBitcoin.gbpRate
        usdRateLabel.text = CurrentBitcoin.usdRate
        
        defaults.set(CurrentBitcoin.eurRate, forKey: Rate.Eur.rawValue)
        defaults.set(CurrentBitcoin.gbpRate, forKey: Rate.Gbp.rawValue)
        defaults.set(CurrentBitcoin.usdRate, forKey: Rate.Usd.rawValue)
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
}
