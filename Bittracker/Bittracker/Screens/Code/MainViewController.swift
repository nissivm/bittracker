//
//  MainViewController.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import BittrackerCurrent

class MainViewController: UIViewController {
    
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var eurRateLabel: UILabel!
    @IBOutlet weak var gbpRateLabel: UILabel!
    @IBOutlet weak var usdRateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "PreviousRateCell", bundle: nil), forCellReuseIdentifier: "PreviousRateCell")

        NotificationCenter.default.addObserver(self,
        selector: #selector(updateUI),
        name: Notification.Name(RateUpdater.notification),
        object: nil)
        
        let tapGestRec = UITapGestureRecognizer(target: self, action: #selector(openDetail))
        todayView.addGestureRecognizer(tapGestRec)
        
        RateUpdater.startUpdating(interval: 60)
        
        HistoricalBitcoin.get(completion: {
            [unowned self]() -> Void in
            self.tableView.reloadData()
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateUI() {
        
        eurRateLabel.text = CurrentBitcoin.eurRate
        gbpRateLabel.text = CurrentBitcoin.gbpRate
        usdRateLabel.text = CurrentBitcoin.usdRate
    }
    
    @objc func openDetail() {
        
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.date = "Today"
        vc.eur = CurrentBitcoin.eurRate
        vc.gbp = CurrentBitcoin.gbpRate
        vc.usd = CurrentBitcoin.usdRate
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if HistoricalBitcoin.eurData.count > 0 {
            return HistoricalBitcoin.eurData[0].count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreviousRateCell", for: indexPath) as! PreviousRateCell
        
        cell.rateDateLabel.text = HistoricalBitcoin.eurData[0][indexPath.row]
        cell.eurRateLabel.text = HistoricalBitcoin.eurData[1][indexPath.row]
        cell.gbpRateLabel.text = HistoricalBitcoin.gbpData[1][indexPath.row]
        cell.usdRateLabel.text = HistoricalBitcoin.usdData[1][indexPath.row]
        
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.date = HistoricalBitcoin.eurData[0][indexPath.row]
        vc.eur = HistoricalBitcoin.eurData[1][indexPath.row]
        vc.gbp = HistoricalBitcoin.gbpData[1][indexPath.row]
        vc.usd = HistoricalBitcoin.usdData[1][indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
