//
//  DetailViewController.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 26/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import BittrackerCurrent

class DetailViewController: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var eurLabel: UILabel!
    @IBOutlet weak var gbpLabel: UILabel!
    @IBOutlet weak var usdLabel: UILabel!
    
    var date = ""
    var eur = ""
    var gbp = ""
    var usd = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        if date == "Today" {
            NotificationCenter.default.addObserver(self,
            selector: #selector(updateUI),
            name: Notification.Name(RateUpdater.notification),
            object: nil)
        }
        
        dateLabel.text = date
        eurLabel.text = eur
        gbpLabel.text = gbp
        usdLabel.text = usd
    }
    
    deinit {
        if date == "Today" {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    @objc func updateUI() {
        
        eurLabel.text = CurrentBitcoin.eurRate
        gbpLabel.text = CurrentBitcoin.gbpRate
        usdLabel.text = CurrentBitcoin.usdRate
    }
}
