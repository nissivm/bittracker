//
//  DateCreator.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct DateCreator {
    
    static func getPreviousDate(daysBefore: Int) -> Date {
        
        if let d = Calendar.current.date(byAdding: .day, value: daysBefore * -1, to: Date()) {
            return d
        }
        return Date()
    }
}
