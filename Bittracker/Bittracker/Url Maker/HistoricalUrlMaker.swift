//
//  HistoricalUrlMaker.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

enum Currency: String {
    case EUR = "EUR"
    case GBP = "GBP"
    case USD = "USD"
}

struct HistoricalUrlMaker {
    
    static func getRateUrl(currency: Currency) -> URL? {
        
        let twoWeeksAgo = DateCreator.getPreviousDate(daysBefore: 13)
        let start = DateFormater.getUrlFormat(twoWeeksAgo)
        let yesterday = DateCreator.getPreviousDate(daysBefore: 1)
        let end = DateFormater.getUrlFormat(yesterday)
        
        let urlStr = "https://api.coindesk.com/v1/bpi/historical/close.json?start=\(start)&end=\(end)&currency=\(currency.rawValue)"
        
        return URL(string: urlStr)
    }
}
