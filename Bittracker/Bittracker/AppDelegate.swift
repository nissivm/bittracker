//
//  AppDelegate.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import BittrackerCurrent

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.backgroundColor = UIColor.white
        window!.makeKeyAndVisible()
        window!.rootViewController = NavigationControllerConfig.get()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {
        RateUpdater.stopUpdating()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        RateUpdater.startUpdating(interval: 60)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
}

