//
//  UIMonthFormater.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 29/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct UIMonthFormater {
    
    static func getFormated(_ toFormat: String) -> String {
        
        switch toFormat {
        case "01":
            return "Jan"
        case "02":
            return "Fev"
        case "03":
            return "Mar"
        case "04":
            return "April"
        case "05":
            return "May"
        case "06":
            return "Jun"
        case "07":
            return "Jul"
        case "08":
            return "Ago"
        case "09":
            return "Set"
        case "10":
            return "Oct"
        case "11":
            return "Nov"
        case "12":
            return "Dec"
        default:
            return "---"
        }
    }
}
