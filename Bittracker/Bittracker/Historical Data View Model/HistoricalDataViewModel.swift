//
//  HistoricalDataViewModel.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 29/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

struct HistoricalDataViewModel {
    
    static func getViewData(_ decodedBpi: [String : CGFloat]) -> [[String]] {
        
        let sortedDates = DateStringSorter.sort(Array(decodedBpi.keys))
        var formatedSortedDates = [String]()
        var sortedRates = [String]()
        
        for date in sortedDates {
            formatedSortedDates.append(getFormatedDate(date))
            if let rate = decodedBpi[date] {
                sortedRates.append("\(rate)")
            }
            else {
                sortedRates.append("---")
            }
        }
        
        return [formatedSortedDates, sortedRates]
    }
    
    static private func getFormatedDate(_ toFormat: String) -> String {
        
        let arr = toFormat.components(separatedBy: "-")
        let day = arr[2]
        let month = UIMonthFormater.getFormated(arr[1])
        let year = arr[0]
        
        return "\(month) \(day), \(year)"
    }
}
