//
//  HistoricalBitcoin.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

struct HistoricalBitcoin {
    
    static var eurData = [[String]]()
    static var gbpData = [[String]]()
    static var usdData = [[String]]()
    static private let group = DispatchGroup()
    
    static func get(completion: @escaping () -> Void) {
        
        group.enter()
        getEur()
        group.enter()
        getGbp()
        group.enter()
        getUsd()
        
        group.notify(queue: .main) {
            completion()
        }
    }
    
    static private func getEur() {
        
        guard let url = HistoricalUrlMaker.getRateUrl(currency: .EUR) else {
            group.leave()
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) {
            
            data, response, error in
            
            guard let data = data else {
                group.leave()
                return
            }
            
            do {
                
                let decoded = try JSONDecoder().decode(BpiHistorical.self, from: data)
                eurData = HistoricalDataViewModel.getViewData(decoded.bpi)
                group.leave()
                
            } catch {
                //print("JSON decoding error: \(error.localizedDescription)")
                group.leave()
            }
        }
        
        task.resume()
    }
    
    static private func getGbp() {
        
        guard let url = HistoricalUrlMaker.getRateUrl(currency: .GBP) else {
            group.leave()
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else {
                group.leave()
                return
            }
            
            do {
                
                let decoded = try JSONDecoder().decode(BpiHistorical.self, from: data)
                gbpData = HistoricalDataViewModel.getViewData(decoded.bpi)
                group.leave()
                
            } catch {
                //print("JSON decoding error: \(error.localizedDescription)")
                group.leave()
            }
        }
        
        task.resume()
    }
    
    static private func getUsd() {
        
        guard let url = HistoricalUrlMaker.getRateUrl(currency: .USD) else {
            group.leave()
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else {
                group.leave()
                return
            }
            
            do {
                
                let decoded = try JSONDecoder().decode(BpiHistorical.self, from: data)
                usdData = HistoricalDataViewModel.getViewData(decoded.bpi)
                group.leave()
                
            } catch {
                //print("JSON decoding error: \(error.localizedDescription)")
                group.leave()
            }
        }
        
        task.resume()
    }
}
