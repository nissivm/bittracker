//
//  DateStringSorter.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 28/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct DateStringSorter {
    
    static func sort(_ toSort: [String]) -> [String] {
        return toSort.sorted {$0.localizedStandardCompare($1) == .orderedDescending}
    }
}
