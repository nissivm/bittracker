//
//  BpiHistorical.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

struct BpiHistorical: Codable {
    
    var bpi: [String : CGFloat]
}
