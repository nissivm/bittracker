//
//  NavigationControllerConfig.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 26/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

struct NavigationControllerConfig {
    
    static func get() -> UINavigationController {
        
        let vc = MainViewController(nibName: "MainViewController", bundle: nil)
        let navController = UINavigationController(rootViewController: vc)
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.barTintColor = UIColor(red: 33/255, green: 35/255, blue: 38/255, alpha: 1)
        navController.navigationBar.tintColor = UIColor.white
        navController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        return navController
    }
}
