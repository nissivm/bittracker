//
//  RateUpdater.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

public class RateUpdater {
    
    static public let notification = "BitcoinRateUpdatedNotification"
    static private var timer: Timer?
    static private var intv: TimeInterval = 0
    
    static public func startUpdating(interval: TimeInterval) {
        
        stopUpdating() // In case this function is called with a timer still in run loop
        intv = interval
        
        CurrentBitcoin.get(completion: {
            
            () -> Void in
            
            DispatchQueue.main.async {
                
                if CurrentBitcoin.success {
                    NotificationCenter.default.post(name: Notification.Name(notification), object: nil)
                }
                
                timer = Timer.scheduledTimer(timeInterval: intv, target: self, selector: #selector(timerFired), userInfo: nil, repeats: false)
            }
        })
    }
    
    @objc static private func timerFired() {
        timer = nil
        startUpdating(interval: intv)
    }
    
    static public func stopUpdating() {
        
        if let t = timer {
            t.invalidate()
            timer = nil
        }
    }
}
