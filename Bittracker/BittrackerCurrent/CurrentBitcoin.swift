//
//  CurrentBitcoin.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

public struct CurrentBitcoin {
    
    static public private(set) var success = false
    static public private(set) var eurRate = ""
    static public private(set) var gbpRate = ""
    static public private(set) var usdRate = ""
    
    static public func get(completion: (() -> Void)?) {
        
        guard let url = CurrentUrlMaker.getRateUrl() else {
            
            success = false
            if let comp = completion {
                comp()
            }
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else {
                
                success = false
                if let comp = completion {
                    comp()
                }
                return
            }
            
            do {
                
                let decoded = try JSONDecoder().decode(BpiCurrent.self, from: data)
                eurRate = decoded.bpi.EUR.rate
                gbpRate = decoded.bpi.GBP.rate
                usdRate = decoded.bpi.USD.rate
                
                success = true
                
                if let comp = completion {
                    comp()
                }
                
            } catch {
                //print("JSON decoding error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
    }
}
