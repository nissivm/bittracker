//
//  CurrentUrlMaker.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct CurrentUrlMaker {
    
    static private var url: URL?
    
    static func getRateUrl() -> URL? {
        
        if let u = url {
            return u
        }
        
        url = URL(string: "https://api.coindesk.com/v1/bpi/currentprice.json")
        
        return url
    }
}
