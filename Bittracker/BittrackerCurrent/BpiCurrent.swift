//
//  BpiCurrent.swift
//  Bittracker
//
//  Created by Nissi Vieira Miranda on 25/05/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct BpiCurrent: Codable {
    
    struct Bpi: Codable {
        
        struct Eur: Codable {
            var rate: String
        }
        struct Gbp: Codable {
            var rate: String
        }
        struct Usd: Codable {
            var rate: String
        }
        
        var EUR: Eur
        var GBP: Gbp
        var USD: Usd
    }
    
    var bpi: Bpi
}
