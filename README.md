# README #

- The application reads data from Coindesk API and shows current rates (every 60 sec) of Bitcoin for Euro, British Pound and US Dollar.
- The app also shows historical data (13 days ago) for the same currencies.

- The main north for creating app's architecture and code was SOLID'S Single Responsability Principle.
- The app also uses MVVM architecture, for parsing the historical data.
- No third party code was used for building this application.

- The application contains a Today Widget(BittrackerToday), for showing current rates (every 10 sec).
- The application also contains a Framework(BittrackerCurrent), for sharing code between the main app and the widget, which is the code related updating current rates.

